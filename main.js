const puppeteer = require('puppeteer');

var arg = process.argv
var exepath = '/usr/bin/chromium'
var url = arg[2];
var imagename = arg[3] + '.png'
var proxy_adr = '--proxy-server=127.0.0.1:' + arg[4]
var ip_adr = arg[5];

puppeteer.launch({executablePath: exepath, args: [proxy_adr]}).then(async browser => {
  const page = await browser.newPage();

  await page.setRequestInterception(true);
  page.on('request', interceptedRequest =>{
    console.log("Real Request: " + interceptedRequest.url().toString());

    if (interceptedRequest.url().startsWith('https')) {
      var newurl = new URL(interceptedRequest.url());
      newurl.protocol = 'http:';
      console.log(" What sent: " + newurl.toString() + "\n");

      interceptedRequest.continue({
        url: newurl.toString(),
        method: 'GET',
        headers: {'X-Is-Https': 'true', 'Accept': '*/*',
                  'X-Ip-Address' : ip_adr}
      });
    }
    else if (interceptedRequest.url().startsWith('http')) {
      console.log(" What sent: " + interceptedRequest.url().toString() + "\n");
      interceptedRequest.continue({
        url: interceptedRequest.url(),
        method: 'GET',
        headers: {'X-Is-Https': 'false', 'Accept': '*/*',
                  'X-Ip-Address' : ip_adr}
      });
    }
    else {
      console.log("Aborting Request \n");
      interceptedRequest.abort();
    }
  });

  page.on("response", response => {
    const request = response.request();
    const url = request.url();
    const status = response.status();
    console.log("response url: " + url + " status: " + status + '\n');
  });

  page.on('requestfailed', interceptedRequest => {
    console.log("ERROR : " + interceptedRequest.url() + ' ' + interceptedRequest.failure().errorText);
    interceptedRequest.continue();
  });

  try {
    await page.goto(url, {timeout: 90000, waitUntil: 'networkidle0'});
  }
  catch(error) {
    console.log(error);
  }

  console.log("Page.goto() finished!");
  await page.screenshot({path: imagename, fullPage: true});
  console.log("Captured Screenshot");
  await browser.close();

});
